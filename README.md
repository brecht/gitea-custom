# Gitea Customization for Blender Developer Portal

## Setup

- Clone this repo in a `custom` folder at the root of your gitea install.
- Edit app.ini to set the following in the `[ui]` section:
  - `DEFAULT_THEME = bthree-dark`
  - `THEMES = bthree-dark`
- Run the gitea binary with the following arguments (or do the clone into Gitea's own `custom` directory, merging with your `conf` dir).
  - --custom-path pointing to the repo directory
  - --config pointing to the custom app.ini
- Update your user preferences to set the new theme, or it will keep showing the old one.
